use std::error::Error;
use std::fmt::{Display, Formatter};
use std::marker::PhantomData;

use super::formulas::Formula;
use super::metric::{Bottom, Top};
use super::trace::Trace;

#[derive(Debug)]
pub enum LiteralError {}

impl Display for LiteralError {
    fn fmt(&self, _f: &mut Formatter<'_>) -> std::fmt::Result {
        unreachable!()
    }
}

impl Error for LiteralError {}

pub struct True<T> {
    metric_type: PhantomData<T>,
}

impl<T> True<T> {
    pub fn new() -> Self {
        Self {
            metric_type: PhantomData,
        }
    }
}

impl<S, T> Formula<S> for True<T>
where
    T: Top,
{
    type Metric = T;
    type Error = LiteralError;

    fn evaluate_trace(&self, trace: &Trace<S>) -> Result<Trace<Self::Metric>, Self::Error> {
        Ok(trace.iter().map(|(time, _)| (time, T::top())).collect())
    }
}

pub struct False<T> {
    metric_type: PhantomData<T>,
}

impl<T> False<T> {
    pub fn new() -> Self {
        Self {
            metric_type: PhantomData,
        }
    }
}

impl<S, T> Formula<S> for False<T>
where
    T: Bottom,
{
    type Metric = T;
    type Error = LiteralError;

    fn evaluate_trace(&self, trace: &Trace<S>) -> Result<Trace<Self::Metric>, Self::Error> {
        Ok(trace.iter().map(|(time, _)| (time, T::bottom())).collect())
    }
}

pub struct Literal<T> {
    trace: Trace<T>,
}

impl<T> Literal<T> {
    pub fn new(trace: Trace<T>) -> Self {
        Self { trace }
    }
}

impl<S, T> Formula<S> for Literal<T>
where
    T: Clone,
{
    type Metric = T;
    type Error = LiteralError;

    fn evaluate_trace(&self, trace: &Trace<S>) -> Result<Trace<Self::Metric>, Self::Error> {
        Ok(self.trace.clone())
    }
}

#[cfg(test)]
mod tests {
    use super::{False, True};
    use crate::expressions::{Predicate, Variables};
    use crate::formulas::Formula;
    use crate::operators::{And, Or};
    use crate::trace::Trace;

    fn get_trace() -> Trace<Variables> {
        Trace::from_iter([
            (0.0, Variables::from([("x", 1.0)])),
            (0.5, Variables::from([("x", 2.0)])),
            (1.0, Variables::from([("x", 3.0)])),
            (1.5, Variables::from([("x", 4.0)])),
        ])
    }

    #[test]
    fn test_true() -> Result<(), Box<dyn std::error::Error>> {
        let trace = get_trace();
        let formula = And::new(Predicate::simple("x", 1.0, 5.0), True::new());
        let evaluation = formula.evaluate_trace(&trace)?;

        assert_eq!(evaluation[0.0], 4.0);
        assert_eq!(evaluation[0.5], 3.0);
        assert_eq!(evaluation[1.0], 2.0);
        assert_eq!(evaluation[1.5], 1.0);

        Ok(())
    }

    #[test]
    fn test_false() -> Result<(), Box<dyn std::error::Error>> {
        let trace = get_trace();
        let formula = Or::new(Predicate::simple("x", 1.0, 5.0), False::new());
        let evaluation = formula.evaluate_trace(&trace)?;

        assert_eq!(evaluation[0.0], 4.0);
        assert_eq!(evaluation[0.5], 3.0);
        assert_eq!(evaluation[1.0], 2.0);
        assert_eq!(evaluation[1.5], 1.0);

        Ok(())
    }
}
